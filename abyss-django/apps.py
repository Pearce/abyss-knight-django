from django.apps import AppConfig


class AbyssKnightConfig(AppConfig):
    name = 'abyss-django'