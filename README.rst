=====
abyss-django
=====

Abyss Knight is a 2d rouguelike platformer developed in Unity2d. This repo is a WebGL build of the game configured to work as a django app.

All art by Hannah Fullerton.

Quick start
-----------

1. Add "polls" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'abyss-django',
    ]

2. Include the polls URLconf in your project urls.py like this::

    path('abyss-django/', include('abyss-django.urls')),
